/*
 * GStreamer
 * Copyright (C) 2015 Matthew Waters <matthew@centricular.com>
 * Copyright (C) 2021 Collabora Ltd.
 *   @author George Kiagiadakis <george.kiagiadakis@collabora.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstgtkwaylandsink.h"
#include "gtkgstwaylandwidget.h"
#include "gstgtkutils.h"

#include <gst/video/video.h>
#include <gdk/gdk.h>

// This is <gst/wayland/wayland.h>
// Included like that because of broken gstreamer installation in the STM SDK<
#include <wayland/wayland.h>

#ifdef GDK_WINDOWING_WAYLAND
#include <gdk/gdkwayland.h>
#else
#error "Wayland is not supported in GTK+"
#endif

GST_DEBUG_CATEGORY (gst_debug_gtk_wayland_sink);
#define GST_CAT_DEFAULT gst_debug_gtk_wayland_sink

#ifndef GST_CAPS_FEATURE_MEMORY_DMABUF
#define GST_CAPS_FEATURE_MEMORY_DMABUF "memory:DMABuf"
#endif

#define WL_VIDEO_FORMATS \
  "{ BGRx, BGRA, RGBx, xBGR, xRGB, RGBA, ABGR, ARGB, RGB, BGR, " \
  "RGB16, BGR16, YUY2, YVYU, UYVY, AYUV, NV12, NV21, NV16, NV61, " \
  "YUV9, YVU9, Y41B, I420, YV12, Y42B, v308 }"

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (WL_VIDEO_FORMATS) ";"
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES (GST_CAPS_FEATURE_MEMORY_DMABUF,
            WL_VIDEO_FORMATS))
    );

static void
gst_gtk_wayland_sink_navigation_interface_init (GstNavigationInterface * iface);

static GstPadProbeReturn sink_pad_probe_cb (GstPad * pad,
    GstPadProbeInfo * info, gpointer user_data);

enum
{
  PROP_0,
  PROP_WIDGET,
};

#define gst_gtk_wayland_sink_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (GstGtkWaylandSink, gst_gtk_wayland_sink, GST_TYPE_BIN,
    G_IMPLEMENT_INTERFACE (GST_TYPE_NAVIGATION,
        gst_gtk_wayland_sink_navigation_interface_init);
    GST_DEBUG_CATEGORY_INIT (gst_debug_gtk_wayland_sink, "gtkwaylandsink", 0,
        "Gtk Wayland Video Sink"));

static void
gst_gtk_wayland_sink_init (GstGtkWaylandSink * self)
{
  self->waylandsink = gst_element_factory_make ("waylandsink", NULL);
  if (self->waylandsink) {
    GstPadTemplate *tmpl;
    GstPad *pad;

    gst_bin_add (GST_BIN (self), self->waylandsink);

    tmpl = gst_static_pad_template_get (&sink_template);
    pad = gst_element_get_static_pad (self->waylandsink, "sink");
    self->ghostpad = gst_ghost_pad_new_from_template ("sink", pad, tmpl);

    gst_pad_add_probe (self->ghostpad, GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM,
        sink_pad_probe_cb, self, NULL);
    gst_element_add_pad (GST_ELEMENT (self), self->ghostpad);

    gst_object_unref (pad);
    gst_object_unref (tmpl);
  }
}

static void
gst_gtk_wayland_sink_finalize (GObject * object)
{
  GstGtkWaylandSink *self = GST_GTK_WAYLAND_SINK (object);

  g_clear_object (&self->widget);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
widget_destroy_cb (GtkWidget * widget, GstGtkWaylandSink * self)
{
  GST_OBJECT_LOCK (self);
  g_clear_object (&self->widget);
  GST_OBJECT_UNLOCK (self);
}

static void
window_destroy_cb (GtkWidget * widget, GstGtkWaylandSink * self)
{
  GST_OBJECT_LOCK (self);
  self->window = NULL;
  GST_OBJECT_UNLOCK (self);

  GST_ELEMENT_ERROR (self, RESOURCE, NOT_FOUND, ("Window was closed"), (NULL));
}

/* We use the "draw" callback to change the size of the sink
 * because the "configure-event" is only sent to top-level widgets. */
static gboolean
widget_draw_cb (GtkWidget * widget, cairo_t * cr, gpointer user_data)
{
  GstGtkWaylandSink *self = user_data;
  GtkAllocation allocation;

  gtk_widget_get_allocation (widget, &allocation);
  gst_video_overlay_set_render_rectangle (GST_VIDEO_OVERLAY (self->waylandsink),
      allocation.x, allocation.y, allocation.width, allocation.height);

  return FALSE;
}

static GtkWidget *
gst_gtk_wayland_sink_get_widget (GstGtkWaylandSink * self)
{
  if (self->widget != NULL)
    return g_object_ref (self->widget);

  /* Ensure GTK is initialized, this has no side effect if it was already
   * initialized. Also, we do that lazily, so the application can be first */
  if (!gtk_init_check (NULL, NULL)) {
    GST_INFO_OBJECT (self, "Could not ensure GTK initialization.");
    return NULL;
  }

  self->widget = gtk_gst_wayland_widget_new ();
  g_signal_connect_object (self->widget, "draw",
      G_CALLBACK (widget_draw_cb), self, 0);
  gtk_gst_base_widget_set_element (GTK_GST_BASE_WIDGET (self->widget),
      GST_ELEMENT (self));

  /* Take the floating ref, other wise the destruction of the container will
   * make this widget disappear possibly before we are done. */
  g_object_ref_sink (self->widget);
  g_signal_connect_object (self->widget, "destroy",
      G_CALLBACK (widget_destroy_cb), self, 0);

  return g_object_ref (self->widget);
}

static GtkWidget *
gst_gtk_wayland_sink_acquire_widget (GstGtkWaylandSink * self)
{
  gpointer widget = NULL;

  GST_OBJECT_LOCK (self);
  if (self->widget != NULL)
    widget = g_object_ref (self->widget);
  GST_OBJECT_UNLOCK (self);

  if (!widget)
    widget =
        gst_gtk_invoke_on_main ((GThreadFunc) gst_gtk_wayland_sink_get_widget,
        self);

  return widget;
}

static void
gst_gtk_wayland_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstGtkWaylandSink *self = GST_GTK_WAYLAND_SINK (object);

  switch (prop_id) {
    case PROP_WIDGET:
    {
      g_value_take_object (value, gst_gtk_wayland_sink_acquire_widget (self));
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_gtk_wayland_sink_start_on_main (GstGtkWaylandSink * self)
{
  GtkWidget *toplevel;
  GstContext *context;
  GdkDisplay *display;
  struct wl_display *display_handle;

  if ((toplevel = gst_gtk_wayland_sink_get_widget (self)) == NULL) {
    GST_ERROR_OBJECT (self, "Could not ensure GTK initialization.");
    return FALSE;
  }
  g_object_unref (toplevel);

  /* After this point, self->widget will always be set */

  toplevel = gtk_widget_get_toplevel (GTK_WIDGET (self->widget));
  if (!gtk_widget_is_toplevel (toplevel)) {
    /* User did not add widget its own UI, let's popup a new GtkWindow to
     * make gst-launch-1.0 work. */
    self->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (self->window), 640, 480);
    gtk_window_set_title (GTK_WINDOW (self->window), "Gst GTK Wayland Sink");
    gtk_container_add (GTK_CONTAINER (self->window), toplevel);
    self->window_destroy_id = g_signal_connect (self->window, "destroy",
        G_CALLBACK (window_destroy_cb), self);
  }

  display = gtk_widget_get_display (self->widget);
  if (!GDK_IS_WAYLAND_DISPLAY (display)) {
    GST_ERROR_OBJECT (self, "GDK is not using its wayland backend.");
    return FALSE;
  }
  display_handle = gdk_wayland_display_get_wl_display (display);
  context = gst_wayland_display_handle_context_new (display_handle);
  gst_element_set_context (self->waylandsink, context);
  gst_context_unref (context);

  return TRUE;
}

static gboolean
gst_gtk_wayland_sink_stop_on_main (GstGtkWaylandSink * self)
{
  if (self->window) {
    if (self->window_destroy_id)
      g_signal_handler_disconnect (self->window, self->window_destroy_id);
    self->window_destroy_id = 0;
    gtk_widget_destroy (self->window);
    self->window = NULL;
  }

  return TRUE;
}

static void
gst_gtk_widget_show_all_and_unref (GtkWidget * widget)
{
  gtk_widget_show_all (widget);
  g_object_unref (widget);
}

static GstStateChangeReturn
gst_gtk_wayland_sink_change_state (GstElement * element,
    GstStateChange transition)
{
  GstGtkWaylandSink *self = GST_GTK_WAYLAND_SINK (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      if (!self->waylandsink) {
        GST_ERROR_OBJECT (self, "Failed to load waylandsink");
        return GST_STATE_CHANGE_FAILURE;
      }
      if (!gst_gtk_invoke_on_main ((GThreadFunc)
              gst_gtk_wayland_sink_start_on_main, element))
        return GST_STATE_CHANGE_FAILURE;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
    {
      GtkWindow *window = NULL;

      GST_OBJECT_LOCK (self);
      if (self->window)
        window = g_object_ref (GTK_WINDOW (self->window));
      GST_OBJECT_UNLOCK (self);

      if (window)
        gst_gtk_invoke_on_main ((GThreadFunc) gst_gtk_widget_show_all_and_unref,
            window);

      break;
    }
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret != GST_STATE_CHANGE_SUCCESS)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_NULL:
    case GST_STATE_CHANGE_NULL_TO_NULL:
      gst_gtk_invoke_on_main ((GThreadFunc)
          gst_gtk_wayland_sink_stop_on_main, element);
      break;
    default:
      break;
  }

  return ret;
}

static GstPadProbeReturn
sink_pad_probe_cb (GstPad * pad, GstPadProbeInfo * info, gpointer user_data)
{
  GstGtkWaylandSink *self = GST_GTK_WAYLAND_SINK (user_data);
  if (GST_IS_EVENT (info->data) &&
      GST_EVENT_TYPE (info->data) == GST_EVENT_CAPS) {
    GstVideoInfo v_info;
    GstCaps *caps;
    GtkWidget *widget;

    widget = gst_gtk_wayland_sink_acquire_widget (self);
    if (widget) {
      gst_event_parse_caps (GST_EVENT (info->data), &caps);
      gst_video_info_from_caps (&v_info, caps);
      gtk_gst_base_widget_set_format (GTK_GST_BASE_WIDGET (widget), &v_info);
      g_object_unref (widget);
    }
  }

  return GST_PAD_PROBE_OK;
}

static void
gst_gtk_wayland_sink_set_window_handle_on_main (GstGtkWaylandSink * self)
{
  GtkAllocation allocation;
  GdkWindow *window;
  struct wl_surface *window_handle;

  gtk_widget_get_allocation (self->widget, &allocation);
  window = gtk_widget_get_window (self->widget);
  window_handle = gdk_wayland_window_get_wl_surface (window);

  GST_INFO_OBJECT (self, "setting window handle and size (%d x %d)",
      allocation.width, allocation.height);

  gst_video_overlay_set_window_handle (GST_VIDEO_OVERLAY (self->waylandsink),
      (guintptr) window_handle);
  gst_video_overlay_set_render_rectangle (GST_VIDEO_OVERLAY (self->waylandsink),
      allocation.x, allocation.y, allocation.width, allocation.height);
}

static void
gst_gtk_wayland_sink_handle_message (GstBin * bin, GstMessage * message)
{
  GstGtkWaylandSink *self = GST_GTK_WAYLAND_SINK (bin);

  if (gst_is_video_overlay_prepare_window_handle_message (message)) {
    gst_gtk_invoke_on_main ((GThreadFunc)
        gst_gtk_wayland_sink_set_window_handle_on_main, self);
    gst_message_unref (message);
    return;
  }

  GST_BIN_CLASS (parent_class)->handle_message (bin, message);
}

static void
gst_gtk_wayland_sink_class_init (GstGtkWaylandSinkClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstElementClass *gstelement_class = (GstElementClass *) klass;
  GstBinClass *gstbin_class = (GstBinClass *) klass;

  gobject_class->finalize = gst_gtk_wayland_sink_finalize;
  gobject_class->get_property = gst_gtk_wayland_sink_get_property;
  gstelement_class->change_state = gst_gtk_wayland_sink_change_state;
  gstbin_class->handle_message = gst_gtk_wayland_sink_handle_message;

  g_object_class_install_property (gobject_class, PROP_WIDGET,
      g_param_spec_object ("widget", "Gtk Widget",
          "The GtkWidget to place in the widget hierarchy "
          "(must only be get from the GTK main thread)",
          GTK_TYPE_WIDGET,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  gst_element_class_set_metadata (gstelement_class, "Gtk Wayland Video Sink",
      "Sink/Video",
      "A video sink that renders to a GtkWidget using Wayland API",
      "George Kiagiadakis <george.kiagiadakis@collabora.com>");

  gst_element_class_add_static_pad_template (gstelement_class, &sink_template);
}

static void
gst_gtk_wayland_sink_navigation_send_event (GstNavigation * navigation,
    GstStructure * structure)
{
  GstGtkWaylandSink *sink = GST_GTK_WAYLAND_SINK (navigation);
  GstEvent *event;
  GstPad *pad;
  gdouble x, y;

  if (gst_structure_get_double (structure, "pointer_x", &x) &&
      gst_structure_get_double (structure, "pointer_y", &y)) {
    GtkGstBaseWidget *widget =
        (GtkGstBaseWidget *) gst_gtk_wayland_sink_acquire_widget (sink);
    gdouble stream_x, stream_y;

    if (widget == NULL) {
      GST_ERROR_OBJECT (sink, "Could not ensure GTK initialization.");
      return;
    }

    gtk_gst_base_widget_display_size_to_stream_size (widget,
        x, y, &stream_x, &stream_y);
    gst_structure_set (structure,
        "pointer_x", G_TYPE_DOUBLE, (gdouble) stream_x,
        "pointer_y", G_TYPE_DOUBLE, (gdouble) stream_y, NULL);

    g_object_unref (widget);
  }

  event = gst_event_new_navigation (structure);
  pad = gst_pad_get_peer (sink->ghostpad);

  GST_TRACE_OBJECT (sink, "navigation event %" GST_PTR_FORMAT, structure);

  if (GST_IS_PAD (pad) && GST_IS_EVENT (event)) {
    if (!gst_pad_send_event (pad, gst_event_ref (event))) {
      /* If upstream didn't handle the event we'll post a message with it
       * for the application in case it wants to do something with it */
      gst_element_post_message (GST_ELEMENT_CAST (sink),
          gst_navigation_message_new_event (GST_OBJECT_CAST (sink), event));
    }
    gst_event_unref (event);
    gst_object_unref (pad);
  }
}

static void
gst_gtk_wayland_sink_navigation_interface_init (GstNavigationInterface * iface)
{
  iface->send_event = gst_gtk_wayland_sink_navigation_send_event;
}
