# To use with a SDK
# make -f Makefile.sdk
#
CC ?= gcc

CFLAGS?=-std=c11 -Wall -Werror -O3

CFLAGS_ADDONS = -I. -fPIC
CFLAGS_ADDONS += $(CFLAGS)

LDFLAGS_ADDONS = -lm $(LDFLAGS)

# GTK+3
CFLAGS_ADDONS += $(shell pkg-config --cflags gtk+-3.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs gtk+-3.0)

# gstreamer
CFLAGS_ADDONS += $(shell pkg-config --cflags gstreamer-1.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs gstreamer-1.0)

# gstreamer video
CFLAGS_ADDONS += $(shell pkg-config --cflags gstreamer-video-1.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs gstreamer-video-1.0)

# gstreamer wayland
CFLAGS_ADDONS += $(shell pkg-config --cflags gstreamer-wayland-1.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs gstreamer-wayland-1.0)

# gstreamer plugins base
CFLAGS_ADDONS += $(shell pkg-config --cflags gstreamer-plugins-base-1.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs gstreamer-plugins-base-1.0)

# glib
CFLAGS_ADDONS += $(shell pkg-config --cflags glib-2.0)
LDFLAGS_ADDONS += $(shell pkg-config --libs glib-2.0)

CFLAGS_ADDONS += $(shell pkg-config --cflags wayland-client)
LDFLAGS_ADDONS += $(shell pkg-config --libs wayland-client)

CFLAGS_ADDONS += \
	-DGST_USE_UNSTABLE_API \
	-DPACKAGE="\"STM gtkwaylandsink\"" \
	-DPACKAGE_VERSION=\"0.1\" \
	-DGST_LICENSE=\"LGPL\" \
	-DGST_PACKAGE_NAME="\"STM gtkwaylandsink\"" \
	-DGST_PACKAGE_ORIGIN=\"STM\"

LDFLAGS_ADDONS += -lpthread -shared

EXE = libgstgtkwayland.so

SOURCES = \
		  gstgtkutils.c \
		  gstgtkwaylandsink.c \
		  gstplugin.c \
		  gtkgstbasewidget.c \
		  gtkgstwaylandwidget.c

OBJ = $(SOURCES:.c=.o)

all: $(EXE)

libgstgtkwayland.so: $(OBJ)
	@echo "#########generete $@######################"
	$(CC) -o $@ $^ $(LDFLAGS_ADDONS)

clean:
	rm -f *.o src/*.o $(EXE)


%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS_ADDONS)
